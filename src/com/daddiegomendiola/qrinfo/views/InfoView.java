package com.daddiegomendiola.qrinfo.views;

import java.io.File;
import java.io.InputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;

@SuppressLint("SetJavaScriptEnabled")
public class InfoView extends WebView {

	protected final String javascriptInterface = "qrEdificios";
	private static final String MAIN_DIR = "info";
	private static final String INDEX_FILE = "index.html";

	public InfoView(Context context) {
		super(context);
		this.init(context);

	}

	public InfoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init(context);
	}

	public InfoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.init(context);
	}

	private void init(Context context) {
		this.setJavascript();
		this.reset();
	}

	public void reset() {
		this.loadUrl("file:///android_asset/loading.html");
	}

	private void setJavascript() {
		this.getSettings().setJavaScriptEnabled(true);
		this.addJavascriptInterface(this, javascriptInterface);
	}

	private void loadHTML(String html) {
		this.loadDataWithBaseURL("file:///android_asset/", html, "text/html",
				"utf-8", "");
	}

	public void show(String code) {
		getAndParseInfoTask task = new getAndParseInfoTask(this.getContext());
		task.execute(code);
	}

	// Async Task Class
	class getAndParseInfoTask extends AsyncTask<String, Void, String> {

		private Context context;

		public getAndParseInfoTask(Context context) {
			super();
			this.context = context;
		}

		@Override
		protected String doInBackground(String... params) {
			String codigo = params[0];
			String contenido = readStringFromAssets(codigo);

			return contenido;
		}

		private String readStringFromAssets(String codigo) {
			try {
				String filePath = MAIN_DIR + File.separator + codigo + File.separator + INDEX_FILE;
				InputStream stream = context.getAssets().open(filePath);
				int size = stream.available();
				byte[] buffer = new byte[size];
				stream.read(buffer);
				stream.close();
				return (new String(buffer));
			} catch (Exception e) {
				Log.e("com.daddiegomendiola", e.getMessage());
				return "<html><body align='center'><br><br><h3>�No hay informaci�n!</h3></body></html>";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			loadHTML(result);
		}
	}

}
