package com.daddiegomendiola.qrinfo;

import android.app.Activity;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;

import com.daddiegomendiola.qrinfo.views.InfoView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener;

public class MainActivity extends Activity implements OnQRCodeReadListener {

	private static final int CALL_LIMIT = 20;
	private static final boolean REDIM = false;
	private InfoView myInfoView;
	private QRCodeReaderView myDecoderView;
	private int height = 0;
	private int width = 0;
	private boolean showingInfo = false;
	private int callCounter = 0;
	private String currentCode = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_decoder);

		myDecoderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
		myDecoderView.setOnQRCodeReadListener(this);
		myInfoView = (InfoView) findViewById(R.id.infoView);
	}

	// Called when a QR is decoded
	// "text" : the text encoded in QR
	// "points" : points where QR control points are placed
	@Override
	public void onQRCodeRead(String text, PointF[] points) {
		if (!(this.height > 0 && this.width > 0)) {
			this.height = myDecoderView.getHeight();
			this.width = myDecoderView.getWidth();
			LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);
			lp.setMargins((int) (width * 0.05), (int) (height * 0.05),
					(int) (width * 0.05), (int) (height * 0.05));
			this.myInfoView.setLayoutParams(lp);
		}

		if (!(this.showingInfo && this.currentCode.equals(text))){
			if (!this.currentCode.equals(text)){
				this.myInfoView.reset();
			}
			this.myInfoView.setVisibility(View.VISIBLE);
			this.myInfoView.show(text);
			this.currentCode = text;
			this.showingInfo = true;
		}
		
		this.callCounter = 0;
		
		if (REDIM) {
			// Coordenadas de cada cuadrado grande de los qr.
			// Sentido horario, comienza por el de abajo izquierda.
			int y0 = (int) points[0].y;
			int x1 = (int) points[1].x;
			int y1 = (int) points[1].y;
			int x2 = (int) points[2].x;

			int dif1 = (x2 - x1) / 2;
			int dif2 = (y0 - y1) / 2;

			int left = (x1 - dif1);
			int top = (y1 - dif2);
			int right = (width - x2 - dif1);
			int bottom = (height - y0 - dif2);
			LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);
			lp.setMargins(left, top, right, bottom);
			this.myInfoView.setLayoutParams(lp);
		}
	}

	// Called when your device have no camera
	@Override
	public void cameraNotFound() {

	}

	// Called when there's no QR codes in the camera preview image
	@Override
	public void QRCodeNotFoundOnCamImage() {
		this.callCounter = this.callCounter + 1;
		if (callCounter > CALL_LIMIT) {
			this.showingInfo = false;
			this.myInfoView.setVisibility(View.GONE);
			this.myInfoView.reset();
			this.callCounter = 0;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		myDecoderView.getCameraManager().startPreview();
	}

	@Override
	protected void onPause() {
		super.onPause();
		myDecoderView.getCameraManager().stopPreview();
	}
}
